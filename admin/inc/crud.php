<?php
include_once('koneksi.php');

class Crud extends Koneksi
{
    function __construct()
    {
        parent::__construct();
    }

    public function tampil_data()
    {
        $data = mysqli_query($this->koneksi, "SELECT bmipasien.tanggal AS tanggal, pasien.kode AS kode, pasien.nama AS nama, pasien.gender AS gender, bmi.berat AS berat, bmi.tinggi AS tinggi, bmi.nilaibmi AS nilaibmi, bmi.statusbmi AS statusbmi FROM bmipasien INNER JOIN pasien ON pasien.id = bmipasien.pasien INNER JOIN bmi ON bmi.id = bmipasien.bmi");
        $rows = array();
        while ($row = mysqli_fetch_array($data)) {
            $rows[] = $row;
        }
        return $rows;
    }
}
