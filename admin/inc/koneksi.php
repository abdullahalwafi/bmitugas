<?php 

class Koneksi
{
    private $host = 'localhost';
    private $user = 'root';
    private $pass = '';
    private $db   = 'bmi_tugas';
    
    protected $koneksi;
    function __construct()
    {
        if (!isset($this->koneksi)) {
            $this->koneksi = new mysqli($this->host,$this->user,$this->pass,$this->db);
        }
        if (!$this->koneksi) {
            echo 'Koneksi Gagal';
        }
        return $this->koneksi;
    }
}
